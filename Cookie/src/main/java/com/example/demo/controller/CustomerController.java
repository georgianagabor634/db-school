package com.example.demo.controller;

import com.example.demo.dao.CustomerDAO;
import com.example.demo.exception.AlreadyTakenUsernameException;
import com.example.demo.exception.InvalidUsernameException;
import com.example.demo.model.Customer;
import com.example.demo.repository.CustomerRepository;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerDAO customerDAO;

        @GetMapping("/customer/{id}")
    public ModelAndView getCustomerById(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("id");
        modelAndView.addObject("myId", customerDAO.get(id));
        return modelAndView;
    }

    @GetMapping("/customer")
    public ModelAndView getAllCustomers() {
        ModelAndView modelAndView = new ModelAndView("myThymeleaf");
        modelAndView.addObject("customers", customerDAO.getAllCustomers());
        return modelAndView;
    }

    @GetMapping("/customer/username/filter")
    public ModelAndView getCustomerByUsername(@RequestParam("username") String username) {
        ModelAndView modelAndView = new ModelAndView("id");
        modelAndView.addObject("myId", customerDAO.getAllByUsername(username));
        return modelAndView;
    }

    @GetMapping("/customer/city/filter")
    public ModelAndView getCustomerByCity(@RequestParam("city") String city) {
        ModelAndView modelAndView = new ModelAndView("myThymeleaf");
        modelAndView.addObject("customers", customerDAO.getAllByCity(city));
        return modelAndView;
    }

    @GetMapping("/customer/country/filter")
    public ModelAndView getCustomerByCountry(@RequestParam("country") String country) {
        ModelAndView modelAndView = new ModelAndView("myThymeleaf");
        modelAndView.addObject("customers", customerDAO.getAllByCountry(country));
        return modelAndView;
    }

    @GetMapping("/customer/login/{username}")
    public ModelAndView customerLogin(@PathVariable String username) throws InvalidUsernameException {
        ModelAndView modelAndView= new ModelAndView("login");
        modelAndView.addObject("log", customerDAO.login(username));
        return modelAndView;
    }

    @PostMapping("/customer/register")
    public ModelAndView customerRegister(@RequestBody Customer customer) throws AlreadyTakenUsernameException {
        customerDAO.register(customer);
        ModelAndView modelAndView = new ModelAndView("register");
            modelAndView.addObject("login", customerDAO.get(customer.getId()));
        return modelAndView;
    }

    @PostMapping("/customer")
    public Customer createCustomer(@RequestBody Customer customer) {
        customerDAO.create(customer);
        return customer;
    }

    @PostMapping("/customer/update")
    public Customer updateCustomer(@RequestBody Customer customer) {
        customerDAO.update(customer);
        return customer;
    }

    @DeleteMapping("/customer/{id}")
    public void deleteCustomer(@RequestBody Customer customer) {
        customerDAO.delete(customer);
    }

}
