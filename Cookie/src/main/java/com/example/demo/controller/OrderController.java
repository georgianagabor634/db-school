package com.example.demo.controller;

import com.example.demo.dao.CustomerDAO;
import com.example.demo.dao.OrderDAO;
import com.example.demo.model.Customer;
import com.example.demo.model.Order;
import com.example.demo.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequiredArgsConstructor
public class OrderController{
    private final OrderDAO orderDAO;

    @GetMapping("/orders/{id}")
    public ModelAndView getCustomerById(@PathVariable Integer id)
    {
        ModelAndView modelAndView=new ModelAndView("id");
        modelAndView.addObject("myId", orderDAO.get(id));
        return modelAndView;
    }

    @GetMapping("/orders")
    public ModelAndView getAllOrders() {
        ModelAndView modelAndView = new ModelAndView("myThymeleaf");
        modelAndView.addObject("customers", orderDAO.getAllOrders());
        return modelAndView;
    }

    @PostMapping("/orders")
    public Order createOrder(@RequestBody Order order){
        orderDAO.create(order);
        return order;
    }

    @PostMapping("/orders/update")
    public Order updateOrder(@RequestBody Order order){
        orderDAO.update(order);
        return order;
    }

    @DeleteMapping("/orders/{id}")
    public void deleteOrder(@RequestBody Order order){
        orderDAO.delete(order);
    }

}
