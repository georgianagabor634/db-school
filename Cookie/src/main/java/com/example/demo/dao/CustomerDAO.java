package com.example.demo.dao;

import com.example.demo.exception.AlreadyTakenUsernameException;
import com.example.demo.exception.InvalidUsernameException;
import com.example.demo.model.Customer;
import com.example.demo.repository.CustomerRepository;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseCookie;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RequiredArgsConstructor
@Service
public class CustomerDAO implements DAO<Customer> {
    private final CustomerRepository customerRepository;
    @Override
    public Optional<Customer> get(Integer id) {
        return customerRepository.findById(id);
    }

    public List<Customer> getAllCustomers() {
        List<Customer> customers = new ArrayList<>();
        customerRepository.findAll().iterator().forEachRemaining(customers::add);
        return customers;
    }

    public Customer getAllByUsername(String username) {
        return customerRepository.findByUsername(username);
    }

    public List<Customer> getAllByCity(String city) {
        List<Customer> customers = new ArrayList<>();
        customerRepository.findByCity(city).iterator().forEachRemaining(customers::add);
        return customers;
    }

    public List<Customer> getAllByCountry(String country) {
        List<Customer> customers = new ArrayList<>();
        customerRepository.findByCountry(country).iterator().forEachRemaining(customers::add);
        return customers;
    }

    public Customer login(String username) throws InvalidUsernameException {
        if (customerRepository.findByUsername(username) != null)
            return customerRepository.findByUsername(username);
        else throw new InvalidUsernameException();
    }

    public void register(Customer customer) throws AlreadyTakenUsernameException {
        if (customerRepository.findByUsername(customer.getUsername()) == null) {
            customerRepository.save(customer);
        } else throw new AlreadyTakenUsernameException();
    }

    @Override
    public void create(Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public void delete(Customer customer) {
        customerRepository.delete(customer);
    }

    @Override
    public void update(Customer customer) {
        customerRepository.save(customer);
    }
}
