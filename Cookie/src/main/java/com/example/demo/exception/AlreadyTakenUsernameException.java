package com.example.demo.exception;

public class AlreadyTakenUsernameException extends Exception{
    public AlreadyTakenUsernameException(){
        super("The username you typed is already in use.Please try a different one.");
    }
}
