package com.example.demo.exception;

public class InvalidUsernameException extends Exception {
    public InvalidUsernameException() {
        super("The username you entered is invalid!");
    }
}
