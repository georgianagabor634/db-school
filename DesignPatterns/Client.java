package DB_Teme.Homework.DesignPatterns;

import java.util.Observable;
import java.util.Observer;

public class Client implements Observer  {

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Wow! New pizza!");
    }
}
