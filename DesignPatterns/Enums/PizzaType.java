package DB_Teme.Homework.DesignPatterns.Enums;

public enum PizzaType {
    CAPRICIOSA, MARGUERITA, PROSCIUTTO_FUNGHI, QUATRO_STAGIONI
}
