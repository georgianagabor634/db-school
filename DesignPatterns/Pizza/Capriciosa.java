package DB_Teme.Homework.DesignPatterns.Pizza;

public class Capriciosa extends Pizza{
    private String name="Capriciosa";

    @Override
    public String getName() {
        return name;
    }
}
