package DB_Teme.Homework.DesignPatterns.Pizza;

public class Marguerita extends Pizza{
    private String name="Marguerita";

    @Override
    public String getName() {
        return name;
    }
}
