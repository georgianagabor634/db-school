package DB_Teme.Homework.DesignPatterns.Pizza;

public class ProsciuttoFunghi extends Pizza{
    private String name="ProsciuttoFunghi";

    @Override
    public String getName() {
        return name;
    }
}
