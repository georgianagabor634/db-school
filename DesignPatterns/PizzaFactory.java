package DB_Teme.Homework.DesignPatterns;

import DB_Teme.Homework.DesignPatterns.Enums.PizzaType;
import DB_Teme.Homework.DesignPatterns.Enums.Topping;
import DB_Teme.Homework.DesignPatterns.Pizza.*;

import java.util.Observable;
import java.util.Observer;

public class PizzaFactory extends Observable {

    private static PizzaFactory uniqueInstance;
    PizzaType pizzaTypes;
    Topping toppingTypes;
    public Observer observer;
    private boolean newPizza;

    public void setOutOfDate(boolean newPizza) {
        this.newPizza = newPizza;

        // Tell Java : I have changed
        this.setChanged();

        // Tell Java : Notify my observer(s)
        this.notifyObservers("New pizza arrived!");
    }

    private PizzaFactory() {

    }

    public static PizzaFactory instance() {
        if (uniqueInstance == null) {
            uniqueInstance = new PizzaFactory();
        }
        return uniqueInstance;
    }

    public Pizza createPizza(PizzaType type) {
        switch (type) {
            case CAPRICIOSA:
                this.newPizza=true;
                return new Capriciosa();
            case PROSCIUTTO_FUNGHI:
                this.newPizza=true;
                return new ProsciuttoFunghi();
            case QUATRO_STAGIONI:
                this.newPizza=true;
                return new QuatroStagioni();
            case MARGUERITA:
                this.newPizza=true;
                return new Marguerita();
        }
        return null;
    }
}

class PizzaDecorator extends Pizza {
    protected Pizza pizza;

    public PizzaDecorator(Pizza pizza) {
        this.pizza = pizza;
    }

    public void addTopping(Topping top)
    {
        System.out.println(pizza.getName()+top);
    }
}

class ToppingDecorator extends PizzaDecorator {
    public ToppingDecorator(Pizza pizza) {
        super(pizza);
    }
}
