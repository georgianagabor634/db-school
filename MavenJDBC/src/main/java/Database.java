import java.sql.*;
import java.util.Scanner;

public class Database {
    private Connection connection;

    public Database() throws SQLException {
        connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/customers","root","1234");
    }

    public void insert(Customer customer) throws SQLException {
        PreparedStatement preparedStatement=connection.prepareStatement("INSERT INTO `customers` (`id`,`username`,`last_name`,`first_name`,`phone`,`address`,`city`,`postalCode`,`country`)VALUES (?,?,?,?,?,?,?,?,?);");
        preparedStatement.setInt(1,customer.getId());
        preparedStatement.setString(2,customer.getUsername());
        preparedStatement.setString(3,customer.getLast_name());
        preparedStatement.setString(4,customer.getFirst_name());
        preparedStatement.setString(5,customer.getPhone());
        preparedStatement.setString(6,customer.getAddress());
        preparedStatement.setString(7,customer.getCity());
        preparedStatement.setString(8,customer.getPostalCode());
        preparedStatement.setString(9,customer.getCountry());
        preparedStatement.execute();
    }

    public void getByID(int ID) throws SQLException {
        Statement ps=connection.createStatement();
        ResultSet resultSet=ps.executeQuery("Select * from customers WHERE id="+ID);
        if(resultSet.next())
        {
            System.out.print(resultSet.getString(1)+".");
            System.out.print(resultSet.getString(2)+"\t");
            System.out.print(resultSet.getString(3)+"\t");
            System.out.print(resultSet.getString(4)+"\t");
            System.out.print(resultSet.getString(5)+"\t");
            System.out.print(resultSet.getString(6)+"\t");
            System.out.print(resultSet.getString(7)+"\t");
            System.out.print(resultSet.getString(8)+"\t");
            System.out.print(resultSet.getString(9)+"\t\n\n");

        }
    }
    public void getAll()throws SQLException{
        System.out.println("\n  Here are all the customers you've got:");
        Statement ps=connection.createStatement();
        ResultSet resultSet=ps.executeQuery("Select * from customers");
        while(resultSet.next())
        {
            System.out.print(resultSet.getString(1)+".");
            System.out.print(resultSet.getString(2)+"\t");
            System.out.print(resultSet.getString(3)+"\t");
            System.out.print(resultSet.getString(4)+"\t");
            System.out.print(resultSet.getString(5)+"\t");
            System.out.print(resultSet.getString(6)+"\t");
            System.out.print(resultSet.getString(7)+"\t");
            System.out.print(resultSet.getString(8)+"\t");
            System.out.print(resultSet.getString(9)+"\t\n");
        }
    }

    public void update(String name,int ID) throws SQLException{
        PreparedStatement preparedStatement=connection.prepareStatement("UPDATE `customers` set username=? where id="+ID);
        preparedStatement.setString(1,name);
        preparedStatement.executeUpdate();
    }

    public void updateStatus(int ID) throws SQLException{
        PreparedStatement preparedStatement=connection.prepareStatement("UPDATE `orders` set status=? where id="+ID);
        preparedStatement.setString(1,"delivered");
        preparedStatement.executeUpdate();
    }

    public void updateComment(int ID) throws SQLException{
        Scanner scan=new Scanner(System.in);
        System.out.println("Enter your comment:");
        String comment=scan.nextLine();
        PreparedStatement preparedStatement=connection.prepareStatement("UPDATE `orders` set comments=? where id="+ID);
        preparedStatement.setString(1,comment);
        preparedStatement.executeUpdate();
    }

    public void delete(int ID) throws SQLException{
        PreparedStatement preparedStatement=connection.prepareStatement("DELETE FROM `customers` where id="+ID);
        preparedStatement.execute();
    }

    public void addOrder(Order order)throws SQLException{
        PreparedStatement preparedStatement=connection.prepareStatement("INSERT INTO `orders` (`id`,`order_date`,`shipped_date`,`status`,`comments`,`customer_id`)VALUES (?,?,?,?,?,?);");
        preparedStatement.setInt(1,order.getId());
        preparedStatement.setDate(2, order.getOrder_date());
        preparedStatement.setDate(3, order.getShipped_date());
        preparedStatement.setString(4,order.getStatus());
        preparedStatement.setString(5,order.getComments());
        preparedStatement.setInt(6,order.getCustomer_id());
        preparedStatement.execute();
    }

    public void viewAllOrders(int ID)throws SQLException{
        Statement ps=connection.createStatement();
        ResultSet resultSet=ps.executeQuery("Select * from orders WHERE customer_id="+ID);
        System.out.println("\n The orders of customer "+ID+" are:");
        while(resultSet.next()) {
            System.out.print(resultSet.getString(1) + ".");
            System.out.print(resultSet.getDate(2) + "\t");
            System.out.print(resultSet.getDate(3) + "\t");
            System.out.print(resultSet.getString(4) + "\t");
            System.out.print(resultSet.getString(5) + "\t");
            System.out.print(resultSet.getString(6) + "\t\n");
        }

    }
    public void deleteOrder(int ID) throws SQLException{
        PreparedStatement preparedStatement=connection.prepareStatement("DELETE FROM `orders` where id="+ID);
        preparedStatement.execute();
    }


}
