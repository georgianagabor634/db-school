import java.sql.*;

public class MyJDBC {
    public static void main(String[] args) throws SQLException {
        Database myDatabase = new Database();
         //myDatabase.insert(new Customer(4,"ana_grozava","Ana","Ichim","0735xxxxxx","Str. Bujorului","Bacau","xxxx","Romania"));
        //myDatabase.delete(3);
        myDatabase.update("amidamaru967",2);
        myDatabase.getByID(1);
        myDatabase.getAll();

        //myDatabase.deleteOrder(1);
        //myDatabase.addOrder(new Order(5, new Date(2021, 10, 10), new Date(2021, 11, 10), "loading", "can't wait for my order!", 2));
        myDatabase.viewAllOrders(1);
        //myDatabase.updateStatus(1);
        myDatabase.updateComment(1);
        myDatabase.viewAllOrders(1);

    }
}
