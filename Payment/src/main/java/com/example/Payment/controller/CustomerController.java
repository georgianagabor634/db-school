package com.example.Payment.controller;

import com.example.Payment.model.Customer;
import com.example.Payment.repository.CustomerRepository;
import com.example.Payment.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class CustomerController {
    private final CustomerService customerService;

    @GetMapping("/customers")
    public List<Customer> getAllCustomers() {
        return customerService.getAllCustomers();
    }

    @GetMapping("/customers/{id}")
    public Customer getById(@PathVariable Long id)
    {
        return customerService.getById(id);
    }

    @PostMapping("/customers")
    public Customer createCustomer(@RequestBody Customer customer){
        customerService.insert(customer);
        return customer;
    }
    @PostMapping("/customers/update")
    public Customer updateCustomer(@RequestBody Customer customer){
        customerService.update(customer);
        return customer;
    }

    @DeleteMapping("/customers/{id}")
    public void deleteCustomer(@RequestBody Customer customer){
        customerService.delete(customer);
    }
}
