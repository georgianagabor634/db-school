package com.example.Payment.controller;

import com.example.Payment.model.Order;
import com.example.Payment.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class OrderController {
    private final OrderService orderService;

    @GetMapping("/order/{id}")
    public Order getById(@PathVariable Long id){
        return orderService.getById(id);
    }

    @GetMapping("/order")
    public List<Order> getAllOrders(){
        return orderService.getAllOrders();
    }

    @PostMapping("/order")
    public Order createOrder(@RequestBody Order order){
        orderService.insert(order);
        return order;
    }

    @PostMapping("/order/{id}/{price}/updatePrice")
    public void updateOrder(@PathVariable Long id,@PathVariable Double price){
        orderService.updatePrice(id,price);
    }

    @DeleteMapping("/order")
    public void deleteOrder(@RequestBody Order order){
        orderService.delete(order);
    }

    @GetMapping("/order/{id}/getByCustomerId")
    public List<Order> customerOrders(@PathVariable Long id){
        return orderService.getByCustomerId(id);
    }
}
