package com.example.Payment.controller;

import com.example.Payment.repository.PaymentRepository;
import com.example.Payment.service.PaymentService;
import com.example.Payment.service.PaymentService2;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class PaymentController {
    private final PaymentService paymentService;
    private final PaymentService2 paymentService2;
    @PostMapping
    public String makePayment(@RequestBody Long customerId,@RequestBody Long orderId,@RequestBody Double totalPrice){
        return paymentService.makePayment(customerId, orderId, totalPrice);
    }
    @PostMapping
    public String makePayment2(@RequestBody Long customerId,@RequestBody Long orderId,@RequestBody Double totalPrice){
        return paymentService2.makePayment2(customerId, orderId, totalPrice);
    }
}
