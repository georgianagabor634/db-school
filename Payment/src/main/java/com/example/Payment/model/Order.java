package com.example.Payment.model;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="order_table")
@Data
public class Order {
    @Id
    @GeneratedValue
    @NotNull
    private Long id;
    private double price;
    @ManyToOne
    Customer customer;
    @OneToOne
    Payment payment;
}
