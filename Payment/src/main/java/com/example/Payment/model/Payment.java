package com.example.Payment.model;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Data
public class Payment {
    @Id
    @GeneratedValue
    @NotNull
    private Long id;
    private String paymentMethod;
    @OneToOne
    Order oder;
}
