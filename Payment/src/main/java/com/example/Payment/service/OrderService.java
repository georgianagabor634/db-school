package com.example.Payment.service;

import com.example.Payment.model.Customer;
import com.example.Payment.model.Order;
import com.example.Payment.repository.CustomerRepository;
import com.example.Payment.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;

    public Order getById(Long id){
        return orderRepository.findById(id).get();
    }
    public List<Order> getAllOrders() {
        List<Order> orders = new ArrayList<>();
        orderRepository.findAll().iterator().forEachRemaining(orders::add);
        return orders;
    }
    public void insert(Order order){
        orderRepository.save(order);
    }

    public void updatePrice(Long id,Double price){
        Order myOrder=orderRepository.findById(id).get();
        myOrder.setPrice(price);
        orderRepository.save(myOrder);
    }

    public void delete(Order order) {
        orderRepository.delete(order);
    }

    public List<Order> getByCustomerId(Long id)
    {
        return orderRepository.getByCustomerId(id);
    }
}
