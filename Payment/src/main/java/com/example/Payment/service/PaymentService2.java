package com.example.Payment.service;

import com.example.Payment.model.Payment;
import com.example.Payment.repository.PaymentRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Profile("prod")
@Service
@AllArgsConstructor
public class PaymentService2 {
    private final PaymentRepository paymentRepository;
    public Payment getById(Long id){
        return paymentRepository.findById(id).get();
    }
    public List<Payment> getAllCustomers() {
        List<Payment> payments = new ArrayList<>();
        paymentRepository.findAll().iterator().forEachRemaining(payments::add);
        return payments;
    }
    public void insert(Payment payment){
        paymentRepository.save(payment);
    }

    public void update(Payment payment){
        paymentRepository.save(payment);
    }

    public void delete(Payment payment) {
        paymentRepository.delete(payment);
    }

    public String makePayment2(Long customerId,Long orderId,Double totalPrice){
        return "Customer "+customerId+"payed:"+totalPrice+"for order "+orderId+"with cash";
    }
}
