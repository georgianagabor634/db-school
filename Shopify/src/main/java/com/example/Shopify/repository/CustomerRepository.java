package com.example.Shopify.repository;

import com.example.Shopify.model.Customers;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CrudRepository<Customers,Integer> {
     Customers getById(Integer id);
}
