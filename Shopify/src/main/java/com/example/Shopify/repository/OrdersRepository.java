package com.example.Shopify.repository;

import com.example.Shopify.model.Orders;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrdersRepository extends CrudRepository<Orders,Integer> {
    Orders getById(Integer id);
    List<Orders> getByCustomerId(Integer Id);
}
