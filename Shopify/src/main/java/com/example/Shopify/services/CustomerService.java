package com.example.Shopify.services;

import com.example.Shopify.model.Customers;
import com.example.Shopify.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerService {
    @Autowired
    CustomerRepository customerRepository;

    public Customers getById(Integer id){
        return customerRepository.getById(id);
    }

    public List<Customers> getAllCustomers() {
        List<Customers> customers = new ArrayList<>();
        customerRepository.findAll().iterator().forEachRemaining(customers::add);
        return customers;
    }

    public void insert(Customers customer){
        customerRepository.save(customer);
    }

    public void update(Customers customer){
        customerRepository.save(customer);
    }

    public void delete(Customers customer) {
        customerRepository.delete(customer);
    }
}
