package com.example.Shopify.services;

import com.example.Shopify.model.Orders;
import com.example.Shopify.repository.OrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrdersService {
    @Autowired
    OrdersRepository ordersRepository;

    public Orders getById(Integer id){
        return ordersRepository.getById(id);
    }

    public List<Orders> getAllOrders(){
        List<Orders> aux=new ArrayList<>();
        ordersRepository.findAll().iterator().forEachRemaining(aux::add);
        return aux;
    }

    public void insert(Orders order){
        ordersRepository.save(order);
    }

    public void updateStatus(Integer id,String status){
        Orders order=ordersRepository.getById(id);
        order.setStatus(status);
        ordersRepository.save(order);
    }

    public void delete(Integer id){
        ordersRepository.delete(ordersRepository.getById(id));
    }

    public List<Orders> getByCustomerId(Integer id){
        List<Orders>allOrders=getAllOrders();
        List<Orders> customerOrders= allOrders.stream().filter(orders -> orders.getCustomer_id()==id).collect(Collectors.toList());
        return customerOrders;
    }
}
