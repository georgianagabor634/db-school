package com.example.Stream;

import com.example.Stream.enums.Name;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Random;

@SpringBootApplication
public class StreamApplication {

	public static void main(String[] args) {
		SpringApplication.run(StreamApplication.class, args);
		//System.out.println(Name.values()[new Random().nextInt(Name.values().length)]);
	}

}
