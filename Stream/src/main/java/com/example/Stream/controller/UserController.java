package com.example.Stream.controller;

import com.example.Stream.error.AtLeastThree;
import com.example.Stream.model.User;
import com.example.Stream.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.stream.Stream;

@RestController
@AllArgsConstructor
public class UserController {
    private final UserService userService;

     @PostMapping("/add")
    public User addUser(){
         return userService.addUser();
     }

     @GetMapping("/three")
    public Stream<User> showThree() throws AtLeastThree {
         return userService.threeUsers();
    }

    @GetMapping("/minor")
    public List<User> minorUsers(){
         return userService.minorUser();
    }

    @PostMapping("/double")
    public void doubleAge(){
         userService.doubleAge();
    }

    @GetMapping("/all")
    public List<User> allUsers(){
         return userService.allUsers();
    }

    @GetMapping("/last")
    public void lastUser(){
         userService.lastElement();
    }

    @GetMapping("/smallest")
    public User smallestUser(){
         return userService.smallestAge();
    }

    @GetMapping("/greatest")
    public User greatestUser(){
        return userService.greatestAge();
    }

    @PostMapping("/noDuplicates")
    public void removeDuplicates(){
         userService.removeDuplicates();
    }

    @PostMapping("/over30")
    public List<User> usersOver30(){
         return userService.greaterThan30();
    }
}
