package com.example.Stream.error;

public class AtLeastThree extends Exception{
    public AtLeastThree() {
        super("We couldn't find three users in this list!");
    }
}
