package com.example.Stream.model;

import com.example.Stream.enums.Name;
import com.fasterxml.jackson.annotation.JsonTypeId;
import lombok.Data;

import java.util.Locale;
import java.util.Random;
@Data
public class User {
    private Integer id;
    private String name;
    private int age;
    private static int count = 0;

    public void createUser(){
        this.id=count++;
        this.name= String.valueOf(Name.values()[new Random().nextInt(Name.values().length)]).toLowerCase(Locale.ROOT);
        this.age=new Random().nextInt(101);
    }
    //setName(Name.valueOf(name.toString().toLowerCase()));
}
