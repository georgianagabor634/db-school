package com.example.Stream.service;

import com.example.Stream.error.AtLeastThree;
import com.example.Stream.model.User;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UserService {
    private List<User> userList=new ArrayList<>();

    public User addUser(){
        User newUser=new User();
        newUser.createUser();
        userList.add(newUser);
        return newUser;
    }

    public Stream<User> threeUsers() throws AtLeastThree {
        if(userList.size()>=2)
            return Stream.of(userList.get(0),userList.get(1),userList.get(2));
        else throw new AtLeastThree();
    }

    public List<User> minorUser(){
        return userList.stream().filter(user -> user.getAge()<18).collect(Collectors.toList());
    }

    public void doubleAge(){
         userList.stream().forEach(user -> user.setAge(user.getAge()*2));
    }

    public List<User> allUsers(){
        return userList;
    }

    public void lastElement(){
        User last=userList.stream().reduce((a,b)->b).orElse(null);
        System.out.println("My last element is: "+last);
    }

    public User smallestAge(){
        return userList.stream().min(Comparator.comparingInt(User::getAge)).get();
    }

    public User greatestAge(){
        return userList.stream().max(Comparator.comparingInt(User::getAge)).get();
    }

    public void removeDuplicates(){
        userList=userList.stream().distinct().collect(Collectors.toList());
    }

    public List<User> greaterThan30(){
        List<User> over30=userList.stream().filter(user -> user.getAge()>30).collect(Collectors.toList());
        over30.stream().forEach(user -> user.setName(user.getName().toUpperCase(Locale.ROOT)));
        Collections.sort(over30 ,Comparator.comparingInt(User::getAge).reversed());
        return over30;
    }
}
