package DB_Teme.Homework.Tema2;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        HashMap<Integer, TreeSet<Student>> myMap = new HashMap<>();
        List<Student> students = new ArrayList<>();

        Student s = new Student("Ionel", 10);
        Student s1 = new Student("Marian", 8.3);
        Student s2 = new Student("Marcel", 8.1);
        Student s3 = new Student("Cecilian", 7.6);
        Student s4 = new Student("Gigel", 9.5);
        Student s5 = new Student("Cristi", 6.7);

        students.add(s);
        students.add(s1);
        students.add(s2);
        students.add(s3);
        students.add(s4);
        students.add(s5);

        for (int i = 0; i < 10; i++) {
            TreeSet<Student> tree = new TreeSet<>();
            myMap.put(i + 1, tree);
        }

        TreeSet<Student> tree;
        for (Student st : students) {
            tree = new TreeSet<>();
            tree=myMap.get((int) st.getGrade());
            tree.add(st);
            myMap.put((int) st.getGrade(), tree);
        }

        System.out.println(myMap);
    }
}
