package DB_Teme.Homework.Tema2;

public class Student implements Comparable<Student> {
    private String name;

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", grade=" + grade +
                '}';
    }

    private double grade;

    public Student(String name, double grade) {
        this.name = name;
        this.grade = grade;
        }

    public double getGrade(){
        return Math.round(grade);
    }

    public int compareTo(Student s) {
        return Double.compare(s.grade,this.grade);
    }
}
