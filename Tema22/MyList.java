package DB_Teme.Homework.Tema22;

import java.util.ArrayList;
import java.util.List;

public class MyList<T>{
    private T[] list;
    private int contor=0;

    public MyList(int dimension){
        list=(T[])new Object[dimension];
    }

    public void add(T element) {
        if(contor==list.length){
            T[] copy=(T[])new Object[contor];
            for(int i=0;i<contor;++i)
            {
                copy[i]=list[i];
            }
            list=(T[])new Object[list.length*2];
            for(int i=0;i<contor;++i)
            {
                list[i]=copy[i];
            }
        }
        list[contor++]=element;
    }

    public void print(){
        for(int i=0;i<contor;i++)
            System.out.print(list[i]+"  ");
        System.out.println("\n");
    }

    public boolean lookup(T element)
    {
        for(int i=0;i<contor;i++)
            if(list[i]==element)return true;
        return false;
    }
}
