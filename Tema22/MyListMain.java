package DB_Teme.Homework.Tema22;

public class MyListMain {
    public static void main(String[] args) {
        MyList<Integer> l1=new MyList<>(3);
        l1.add(1);
        l1.add(2);
        l1.add(3);
        l1.add(4);
        System.out.println(l1.lookup(10));
        System.out.println(l1.lookup(3));
        l1.print();

        MyList<String> l2=new MyList<>(3);
        l2.add("a");
        l2.add("b");
        l2.add("c");
        l2.add("d");
        System.out.println(l2.lookup("z"));
        System.out.println(l2.lookup("d"));
        l2.print();
    }
}
